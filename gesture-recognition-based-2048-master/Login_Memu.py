import tkinter as tk
from tkinter import ttk
from tkinter import messagebox

from PIL import ImageTk
from PIL import Image
from ttkbootstrap import Style
from gesture_2048 import Gesture2048Client
from Greedy_main import main
from PDBC import project_msq
from Main_Menu import main_menu
import Register_Memu

class login_menu():
    def test_connecter(self):
        if project_msq.connecter_test():
            self.login_button.config(state=tk.NORMAL)
            self.register_button.config(state=tk.NORMAL)
            messagebox.showinfo("连接成功", "在线测试成功，你可以登录和注册了")
        else:
            messagebox.showerror("连接失败", "连接失败，请检查网络连接")
    def register(self):
        self.login_window.destroy()
        Register_Memu.register_memu()   

    def submit(self):
        username = self.username_entry.get()
        password = self.password_entry.get()
        
        if project_msq.user_exist(username) :
            if project_msq.login(username, password):
                    messagebox.showinfo("登录成功", "欢迎，尊贵的游戏玩家！")
                    self.login_window.destroy()
                    main_menu()
            else:
                    messagebox.showerror("登录失败", "用户名或密码错误")
                    self.login_window.mainloop()
        else:
                messagebox.showerror("登录失败", "用户不存在")
                self.login_window.mainloop()
    def __init__(self):
        self.login_window = tk.Tk()
        self.login_window.title("注册界面")
        self.login_window.geometry("375x600")

        image = tk.PhotoImage(file="../img/2048.png")
        bg_label = tk.Label(self.login_window, image=image)
        bg_label.place(x=0, y=0, relwidth=1, relheight=1)  # 填充整个窗口

        frame = tk.Frame(self.login_window)
        frame.pack(expand='1')

        self.username_label = tk.Label(frame, text="用户名", width='200 ')
        self.username_label.pack(pady=5)

        self.username_entry = tk.Entry(frame)
        self.username_entry.pack(pady=5)
         # 放在第0行第1列

        self.password_label = tk.Label(frame, text="密码", width='12')
        self.password_label.pack(pady=5)


        self.password_entry = tk.Entry(frame, show="*")
        self.password_entry.pack(pady=5)
        
        self.login_button = tk.Button(frame, text="登录", width='12', command=self.submit)
        self.login_button.pack(pady=5)
        self.login_button.config(state=tk.DISABLED)

        self.register_button = tk.Button(frame, text="注册", width='12', command=self.register)
        self.register_button.pack(pady=5)
        self.register_button.config(state=tk.DISABLED)

        self.test_button = tk.Button(frame, text="测试", width='12', command=self.test_connecter)
        self.test_button.pack(pady=5)

        self.login_window.mainloop()
if __name__ == "__main__":
    login_menu()