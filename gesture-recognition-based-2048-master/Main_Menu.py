import tkinter as tk
from tkinter import ttk
from tkinter import messagebox

from PIL import ImageTk
from PIL import Image
from ttkbootstrap import Style
from gesture_2048 import Gesture2048Client
from Greedy_main import main
from PDBC import project_msq

class main_menu():
        
    def __init__(self):
        
        self.root = tk.Tk()
        self.root.title("2048游戏")
        self.root.geometry("400x600")
        self.root.resizable(False, False)

        # 创建样式并设置主题
        self.style = Style(theme='sandstone')
        # 直接换颜色背景
        self.root.configure(background='#ffe0ba')
        # 插入图片背景
        # canvas = tk.Canvas(root, width=600, height=500)
        # bg_image = tk.PhotoImage(file='E:\photo')
        # canvas.create_image(0, 0, anchor=tk.NW, image=bg_image)
        # canvas.pack()
        # 创建标题标签
        self.title_label = tk.Label(self.root, text="2048", font=("Arial", 60))
        self.title_label.place(relx=0.5, rely=0.15, anchor='center')

        # 创建样式对象
        TButton = ttk.Style()
        self.style.configure('TButton', font=("Arial", 20))
        self.style.configure('TButton', background='#654321')
       
        self.timed_button = ttk.Button(self.root, text="计时模式", command=self.timed_mode, width=18, style='TButton')
        self.timed_button.place(relx=0.5, rely=0.3, anchor='center')

        # 创建非计时模式按钮
        self.untimed_button = ttk.Button(self.root, text="普通模式", command=self.untimed_mode, width=18, style='TButton')
        self.untimed_button.place(relx=0.5, rely=0.4, anchor='center')

        # 创建可返回模式按钮
        self.regression_button = ttk.Button(self.root, text="返回模式", command=self.regression_mode, width=18, style='TButton')
        self.regression_button.place(relx=0.5, rely=0.5, anchor='center')

        # 创建AI模式
        self.new_button2 = ttk.Button(self.root, text="挂机模式", command=self.new_command2, width=18, style='TButton')
        self.new_button2.place(relx=0.5, rely=0.6, anchor='center')

        # 创建退出按钮
        self.exit_button = ttk.Button(self.root, text="退出", command=self.close_window, width=18, style='TButton')
        self.exit_button.place(relx=0.5, rely=0.7, anchor='center')

        # 启动事件循环
        self.root.mainloop()
    def timed_mode(self):
            # 在这里处理计时模式
            self.root.destroy()
            self.gesture_game_client = Gesture2048Client()
            self.gesture_game_client.start(2)


    def untimed_mode(self):
            self.gesture_game_client = Gesture2048Client()
            self.gesture_game_client.start(1)


    def regression_mode(self):
            # 在这里处理可返回模式
            self.root.destroy()
            self.gesture_game_client = Gesture2048Client()
            self.gesture_game_client.start(3)





    def new_command2(self):
            self.gesture_game_client = Gesture2048Client()
            self.gesture_game_client.start(4)
            main()





    def close_window(self):
            self.root.destroy()
            # 创建计时模式按钮    

if __name__ == "__main__":
    main_menu()