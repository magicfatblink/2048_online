import tkinter as tk


class TimeManager:
    def __init__(self, root,label_time):
        self.root = root
        self.timer_id = None
        self.elapsed_time = 0  # 用来存储经过的时间
        self.label = label_time  # 假设你有一个标签来显示时间


    def reset_and_start_timer(self):
        self.stop_timer()  # 先停止计时器（如果它正在运行）
        self.reset_timer()  # 重置时间变量和标签显示
        self.start_timer()  # 开始新的计时

    def start_timer(self):
        self.update_timer()

    def stop_timer(self):
        self.root.after_cancel(self.timer_id)

    def reset_timer(self):
        self.elapsed_time = 0  # 重置时间变量
        self.label.config(text=format_time(self.elapsed_time))  # 更新显示

    def update_timer(self):
        self.elapsed_time += 1  # 这里只是一个简单的例子，你可能需要更复杂的逻辑来计算时间
        self.label.config(text=format_time(self.elapsed_time))  # 更新显示
        self.timer_id = self.root.after(1000, self.update_timer)  # 每秒调用一次自己


def format_time(seconds):
    # 这里是一个简单的格式化函数，将秒数转换为分钟和秒
    minutes, seconds = divmod(seconds, 60)
    return f"{minutes:02d}:{seconds:02d}"

