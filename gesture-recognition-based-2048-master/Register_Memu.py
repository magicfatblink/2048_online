import tkinter as tk
from tkinter import ttk
from tkinter import messagebox

from PIL import ImageTk
from PIL import Image
from ttkbootstrap import Style
from gesture_2048 import Gesture2048Client
from Greedy_main import main
from PDBC import project_msq
from Main_Menu import main_menu

class register_memu():
    def register(self):
        username = self.username_entry.get()
        password = self.password_entry.get()
        ensure_password = self.ensure_password_entry.get()
        phonenumber = self.phonenumber_entry.get()

        if password != ensure_password:
            messagebox.showerror("注册失败", "两次密码输入不一致")
            self.login_window.mainloop()
        else:
            if len(phonenumber) != 11 or not phonenumber.isdigit():
                messagebox.showerror("注册失败", "手机号格式不正确")
                self.login_window.mainloop()
            else:
                if project_msq.user_exist(username)==False:
                    messagebox.showerror("注册失败", "用户已存在")
                    self.login_window.mainloop()    
                else:
                    if project_msq.register(username, password, phonenumber):
                        messagebox.showinfo("注册成功", "注册成功，直接进入系统")
                        main_menu()
                        self.login_window.destroy()
                        
                    else:
                        messagebox.showerror("注册失败", "注册失败，请重试")
                        self.login_window.mainloop()
                
    def __init__(self):
        self.login_window = tk.Tk()
        self.login_window.title("注册界面")
        self.login_window.geometry("375x600")

        image = tk.PhotoImage(file="../img/2048.png")
        bg_label = tk.Label(self.login_window, image=image)
        bg_label.place(x=0, y=0, relwidth=1, relheight=1)  # 填充整个窗口

        frame = tk.Frame(self.login_window)
        frame.pack(expand='1')

        self.username_label = tk.Label(frame, text="用户名", width='200 ')
        self.username_label.pack(pady=5)

        self.username_entry = tk.Entry(frame)
        self.username_entry.pack(pady=5)
         # 放在第0行第1列

        self.password_label = tk.Label(frame, text="密码", width='12')
        self.password_label.pack(pady=5)


        self.password_entry = tk.Entry(frame, show="*")
        self.password_entry.pack(pady=5)
        self.ensure_password_entry = tk.Entry(frame, show="*")
        self.ensure_password_entry.pack(pady=5) 
        self.phonenumber_label = tk.Label(frame, text="手机号", width='12')
        self.phonenumber_label.pack(pady=5)
        self.phonenumber_entry = tk.Entry(frame)
        self.phonenumber_entry.pack(pady=5)

        self.register_button = tk.Button(frame, text="注册", width='12', command=self.register)
        self.register_button.pack(pady=5)

        self.login_window.mainloop()

if __name__ == "__main__":
    register_memu()