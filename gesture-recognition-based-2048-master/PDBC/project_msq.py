from PDBC import msq_connecter
#import msq_connecter
def user_exist(username):
    row, results = msq_connecter.search_user(username)
    if row is None:
        return False
    return True

def login(username, password):
    row, results = msq_connecter.search_user(username)
    if row is None:
        return False
    if row[1] == password:
        return True
    return False

def register(username, password, phonenumber):
    if not user_exist(username):
        return False
    msq_connecter.add_user(username, password, phonenumber)
    return True

def connecter_test():
    if msq_connecter.connect_to_database():
        return True
    else:
        return False

if __name__ == "__main__":
    print(user_exist("1"))
    
    print(register("gx", "123456", "1111111111"))
    print(connecter_test())

    